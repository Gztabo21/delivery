export  class  Product {
      id:number;
      name:string;
      star: string;
      dateInitial:string;
      dateEnd:string;
      picture:string;
      description:string;
      moreDetail:string;
      promo:string;
      terms: string;
}