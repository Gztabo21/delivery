import { Component, OnInit } from '@angular/core';
import { Product } from '../core/models/product';
import { ActionSheetController } from '@ionic/angular';
// import { SocialSharing } from '@ionic-native/social-sharing';
import { FriendsComponent } from '../friends/friends.component';
import { ShareComponent } from '../share/share.component';
import {ModalController} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  //product
  products : Product[]=[
    {
      id:1,
      name:'Burgetify',
      star:'4,5',
      dateInitial:'14 de marzo',
      dateEnd:'20 de abril',
      picture:"assets/img/burger.jpg",
      description:'Hamburguesa de 160g con bacon + bebida +papas con cheddar.',
      moreDetail:' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate ipsam doloribus, sit quasi assumenda et quaerat similique tempore expedita recusandae repellendus inventore deleniti nemo eum odio aut enim neque atque',
      promo:' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate ipsam doloribus, sit quasi assumenda et quaerat similique tempore expedita recusandae repellendus inventore deleniti nemo eum odio aut enim neque atque',
      terms:' Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate ipsam doloribus, sit quasi assumenda et quaerat similique tempore expedita recusandae repellendus inventore deleniti nemo eum odio aut enim neque atque',
    }
  ]
//save promo
isSave = false;

slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  constructor(public actionSheetController: ActionSheetController, public modalController : ModalController) {}
  
  ngOnInit(): void {


}
async inviteAFriendModal() {
  const modal = await this.modalController.create({
    component: FriendsComponent,
    cssClass: 'my-custom-class'
  });
  return await modal.present();
}

async shareModal(){
  const modal = await this.modalController.create({
    component: ShareComponent,
    cssClass:'my-custom-class'
  }) 
  return await modal.present();
}

savePromo(){
  !this.isSave ? this.isSave = true : this.isSave = false;
}

// shareSocialNetworks(){
// this.socialSharing.shareViaWhatsApp('Whatapps').then(data=>{
//   console.log(data);
// })
// }
async presentActionSheet() {
  const actionSheet = await this.actionSheetController.create({
    header: '¿A quien vas invitar?',
    cssClass: 'my-custom-class',
    buttons: [{
      text: 'Delete',
      role: 'destructive',
      icon: 'trash',
      handler: () => {
        console.log('Delete clicked');
      }
    }, {
      text: 'fgh',
      icon: 'share',
      handler: () => {
        console.log('Share clicked');
      }
    }, {
      text: 'Play (open modal)',
      icon: 'caret-forward-circle',
      handler: () => {
        console.log('Play clicked');
      }
    }, {
      text: 'Favorite',
      icon: 'heart',
      handler: () => {
        console.log('Favorite clicked');
      }
    }, {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    }]
  });
  await actionSheet.present();
}
  
}
